﻿using Mango.SharedKernel;
using Mango.Web.Core.Models;

namespace Mango.Web.Core.Interfaces;

public interface IProductService
{
    Task<ApiResponseBase<IEnumerable<Product>>> GetAllProductsAsync();
    Task<ApiResponseBase<Product>> GetProductByIdAsync(int id);
    Task<ApiResponseBase<Product>> CreateProductAsync(Product product);
    Task<ApiResponseBase<Product>> UpdateProductAsync(Product product);
    Task<ApiResponseBase<bool>> DeleteProductAsync(int id);
}