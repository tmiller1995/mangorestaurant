﻿using System.Text;
using Mango.SharedKernel;
using Mango.Web.Core.Interfaces;
using Mango.Web.Core.Models;
using Newtonsoft.Json;

namespace Mango.Web.Infrastructure.Services;

public class ProductService : IProductService
{
    private readonly HttpClient _httpClient;

    public ProductService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<ApiResponseBase<IEnumerable<Product>>> GetAllProductsAsync()
    {
        var response = await _httpClient.GetAsync("api/ProductApi/GetAllProducts");
        if (!response.IsSuccessStatusCode) return null!;

        return JsonConvert.DeserializeObject<ApiResponseBase<IEnumerable<Product>>>(
            await response.Content.ReadAsStringAsync())!;
    }

    public async Task<ApiResponseBase<Product>> GetProductByIdAsync(int id)
    {
        var response = await _httpClient.GetAsync($"api/ProductApi/GetProductById/{id}");
        return !response.IsSuccessStatusCode ? null! : JsonConvert.DeserializeObject<ApiResponseBase<Product>>(await response.Content.ReadAsStringAsync())!;
    }

    public async Task<ApiResponseBase<Product>> CreateProductAsync(Product product)
    {
        var response = await _httpClient.PostAsync("api/ProductApi/CreateProduct",
            new StringContent(JsonConvert.SerializeObject(product), Encoding.UTF8, "application/json"));
        if (response.IsSuccessStatusCode)
            return JsonConvert.DeserializeObject<ApiResponseBase<Product>>(await response.Content.ReadAsStringAsync())!;
        var apiResponseBase = new ApiResponseBase<Product> {DisplayMessage = response.ReasonPhrase, IsSuccess = false};
        apiResponseBase.ErrorMessages.Add(response.StatusCode.ToString());
        return apiResponseBase;

    }

    public async Task<ApiResponseBase<Product>> UpdateProductAsync(Product product)
    {
        var response = await _httpClient.PutAsync("api/ProductApi/UpdateProduct",
            new StringContent(JsonConvert.SerializeObject(product), Encoding.UTF8, "application/json"));
        return !response.IsSuccessStatusCode ? null! : JsonConvert.DeserializeObject<ApiResponseBase<Product>>(await response.Content.ReadAsStringAsync())!;
    }

    public async Task<ApiResponseBase<bool>> DeleteProductAsync(int id)
    {
        var response = await _httpClient.DeleteAsync($"api/ProductApi/DeleteProduct/{id}");
        return !response.IsSuccessStatusCode ? null! : JsonConvert.DeserializeObject<ApiResponseBase<bool>>(await response.Content.ReadAsStringAsync())!;
    }
}