using System.Reflection;
using Mango.Web.Core.Interfaces;
using Mango.Web.Core.Models;
using Mango.Web.ViewModels.Product;
using Microsoft.AspNetCore.Mvc;

namespace Mango.Web.Controllers;

public class ProductController : Controller
{
    private readonly IProductService _productService;

    public ProductController(IProductService productService)
    {
        _productService = productService;
    }
        
    public async Task<IActionResult> Index()
    {
        var viewModel = new ProductListViewModel();
        var serviceResponse = await _productService.GetAllProductsAsync();
        if (serviceResponse.IsSuccess)
        {
            viewModel.Products = serviceResponse.Result;
        }
            
        return View(viewModel);
    }

    public async Task<IActionResult> CreateProduct()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> CreateProduct(Product product)
    {
        if (!ModelState.IsValid) return View(product);
        var serviceResponse = await _productService.CreateProductAsync(product);
        if (!serviceResponse.IsSuccess) return View(product);
        return RedirectToAction(nameof(Index));

    }

    public async Task<IActionResult> EditProduct(int id)
    {
        var serviceResponse = await _productService.GetProductByIdAsync(id);
        Product productToEdit;
        if (serviceResponse.IsSuccess)
        {
            productToEdit = serviceResponse.Result;
        }
        else
        {
            return NotFound(id);
        }
        
        return View(productToEdit);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> EditProduct(Product product)
    {
        if (!ModelState.IsValid) return View(product);
        var serviceResponse = await _productService.UpdateProductAsync(product);
        if (serviceResponse.IsSuccess)
        {
            return RedirectToAction(nameof(Index));
        }

        return View(product);
    }

    public async Task<IActionResult> DeleteProduct(int id)
    {
        var serviceResponse = await _productService.DeleteProductAsync(id);
        if (serviceResponse.IsSuccess && serviceResponse.Result)
            return RedirectToAction(nameof(Index));

        return View(nameof(Index));
    }
}