﻿namespace Mango.Web.ViewModels.Product;

public class ProductListViewModel
{
    public ProductListViewModel()
    {
        Products = new List<Core.Models.Product>();
    }
    
    public IEnumerable<Core.Models.Product> Products { get; set; }
}