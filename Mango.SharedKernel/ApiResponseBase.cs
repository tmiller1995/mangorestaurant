﻿namespace Mango.SharedKernel;

public class ApiResponseBase<T>
{
    public ApiResponseBase()
    {
        IsSuccess = true;
        ErrorMessages = new List<string>();
    }
    
    public bool IsSuccess { get; set; }
    public T Result { get; set; } = default!;
    public string DisplayMessage { get; set; } = default!;
    public List<string> ErrorMessages { get; set; }
}