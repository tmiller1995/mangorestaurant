﻿using Autofac;

namespace Mango.Services.ProductApi.Core;

public class AutofacCoreModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        base.Load(builder);
    }
}