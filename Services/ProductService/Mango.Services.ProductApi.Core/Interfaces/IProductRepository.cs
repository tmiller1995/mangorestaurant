﻿using Mango.Services.ProductApi.Core.Models;

namespace Mango.Services.ProductApi.Core.Interfaces;

public interface IProductRepository
{
    Task<IEnumerable<Product>> GetProducts();
    Task<Product> GetProductById(int productId);
    Task<Product> CreateOrUpdateProduct(Product product);
    Task<bool> DeleteProduct(int productId);
}