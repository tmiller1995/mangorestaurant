﻿using Mango.SharedKernel.Entities;

namespace Mango.Services.ProductApi.Core.Models;

public class Product : BaseEntity<int>
{
    public string Name { get; set; } = default!;
    public decimal Price { get; set; }
    public string? Description { get; set; }
    public string? CategoryName { get; set; }
    public string? ImageUrl { get; set; }
}