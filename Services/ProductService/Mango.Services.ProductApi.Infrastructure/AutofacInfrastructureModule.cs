﻿using Autofac;
using Mango.Services.ProductApi.Core.Interfaces;
using Mango.Services.ProductApi.Infrastructure.Repository;

namespace Mango.Services.ProductApi.Infrastructure;

public class AutofacInfrastructureModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterType<ProductRepository>().As<IProductRepository>().InstancePerLifetimeScope();
    }
}