﻿using Mango.Services.ProductApi.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Mango.Services.ProductApi.Infrastructure.Data.EntityConfigurations;

public class ProductEntityConfiguration : IEntityTypeConfiguration<Product>
{
    public void Configure(EntityTypeBuilder<Product> builder)
    {
        builder.HasKey(p => p.Id)
            .IsClustered();

        builder.Property(p => p.Name)
            .HasMaxLength(250)
            .IsRequired();

        builder.Property(p => p.Price)
            .HasPrecision(20, 2);

        builder.Property(p => p.Description)
            .HasMaxLength(1000);

        builder.Property(p => p.CategoryName)
            .HasMaxLength(250);

        builder.Property(p => p.ImageUrl)
            .HasMaxLength(500);
    }
}