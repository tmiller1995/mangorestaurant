﻿using System.Reflection;
using Mango.Services.ProductApi.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.ProductApi.Infrastructure.Data;

public class MangoProductDbContext : DbContext
{
    public MangoProductDbContext(DbContextOptions<MangoProductDbContext> options) : base(options)
    {
    }

    public DbSet<Product> Product { get; set; } = default!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }
}