﻿using Mango.Services.ProductApi.Core.Interfaces;
using Mango.Services.ProductApi.Core.Models;
using Mango.Services.ProductApi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Mango.Services.ProductApi.Infrastructure.Repository;

public class ProductRepository : IProductRepository
{
    private readonly MangoProductDbContext _mangoProductDbContext;

    public ProductRepository(MangoProductDbContext mangoProductDbContext)
    {
        _mangoProductDbContext = mangoProductDbContext;
    }

    public async Task<IEnumerable<Product>> GetProducts()
    {
        var products = await _mangoProductDbContext.Product.AsNoTracking().ToListAsync();
        return products;
    }

    public async Task<Product> GetProductById(int productId)
    {
        var product = await _mangoProductDbContext.Product.FindAsync(productId);
        return product ?? null!;
    }

    public async Task<Product> CreateOrUpdateProduct(Product product)
    {
        Product productToReturn;
        if (product.Id > 0)
        {
            productToReturn = _mangoProductDbContext.Product.Update(product).Entity;
        }
        else
        {
            productToReturn = (await _mangoProductDbContext.Product.AddAsync(product)).Entity;
        }

        await SaveChanges();
        return productToReturn;
    }

    public async Task<bool> DeleteProduct(int productId)
    {
        var productToDelete = await _mangoProductDbContext.Product.FindAsync(productId);
        if (productToDelete is null)
        {
            return false;
        }

        _mangoProductDbContext.Product.Remove(productToDelete);
        return await SaveChanges();
    }

    private async Task<bool> SaveChanges()
    {
        return await _mangoProductDbContext.SaveChangesAsync() > 0;
    }
}