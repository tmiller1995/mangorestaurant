﻿using System.Reflection;
using Mango.Services.ProductApi.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Mango.Services.ProductApi.Infrastructure;

public static class StartupConfiguration
{
    public static void AddDbContext(this IServiceCollection services, string connectionString) =>
        services.AddDbContext<MangoProductDbContext>(options => options.UseSqlServer(connectionString, sqlOptions =>
            sqlOptions.MigrationsAssembly(Assembly.GetAssembly(typeof(StartupConfiguration))?.FullName)));
}