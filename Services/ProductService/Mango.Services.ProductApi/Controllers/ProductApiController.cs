using AutoMapper;
using Mango.Services.ProductApi.ApiDtos;
using Mango.Services.ProductApi.Core.Interfaces;
using Mango.Services.ProductApi.Core.Models;
using Mango.SharedKernel;
using Microsoft.AspNetCore.Mvc;

namespace Mango.Services.ProductApi.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class ProductApiController : ControllerBase
{
    private readonly IProductRepository _productRepository;
    private readonly IMapper _mapper;

    public ProductApiController(IProductRepository productRepository, IMapper mapper)
    {
        _productRepository = productRepository;
        _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> GetAllProducts()
    {
        var response = new ApiResponseBase<IEnumerable<ProductDto>>();
        try
        {
            var products = _mapper.Map<IEnumerable<ProductDto>>(await _productRepository.GetProducts());
            response.Result = products;
            return Ok(response);
        }
        catch (Exception e)
        {
            // TODO: Implement an ILogger
            SetApiResponseError(e, response);
            return BadRequest(response);
        }
    }

    [HttpGet("{id:int}")]
    public async Task<IActionResult> GetProductById(int id)
    {
        var response = new ApiResponseBase<ProductDto>();
        try
        {
            var product = _mapper.Map<ProductDto>(await _productRepository.GetProductById(id));
            response.Result = product;
            return Ok(response);
        }
        catch (Exception e)
        {
            // TODO: Implement an ILogger
            SetApiResponseError(e, response);
            return BadRequest(response);
        }
    }

    [HttpPost]
    public async Task<IActionResult> CreateProduct([FromBody] ProductDto productDto)
    {
        var response = new ApiResponseBase<ProductDto>();
        try
        {
            var product = await _productRepository.CreateOrUpdateProduct(_mapper.Map<Product>(productDto));
            response.Result = _mapper.Map<ProductDto>(product);
            return Ok(response);
        }
        catch (Exception e)
        {
            SetApiResponseError(e, response);
            return BadRequest(response);
        }
    }

    [HttpPut]
    public async Task<IActionResult> UpdateProduct([FromBody] ProductDto productDto)
    {
        var response = new ApiResponseBase<ProductDto>();
        try
        {
            var product = await _productRepository.CreateOrUpdateProduct(_mapper.Map<Product>(productDto));
            response.Result = _mapper.Map<ProductDto>(product);
            return Ok(response);
        }
        catch (Exception e)
        {
            SetApiResponseError(e, response);
            return BadRequest(response);
        }
    }

    [HttpDelete("{id:int}")]
    public async Task<IActionResult> DeleteProduct(int id)
    {
        var response = new ApiResponseBase<bool>();
        try
        {
            var deleted = await _productRepository.DeleteProduct(id);
            response.Result = deleted;
            return Ok(response);
        }
        catch (Exception e)
        {
            SetApiResponseError(e, response);
            return BadRequest(response);
        }
    }

    private static void SetApiResponseError<T>(Exception exception, ApiResponseBase<T> responseBase)
    {
        responseBase.IsSuccess = false;
        responseBase.ErrorMessages.Add(exception.Message);
    }
}