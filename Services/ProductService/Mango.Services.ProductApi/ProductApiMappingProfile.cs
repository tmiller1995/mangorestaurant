﻿using AutoMapper;
using Mango.Services.ProductApi.ApiDtos;
using Mango.Services.ProductApi.Core.Models;

namespace Mango.Services.ProductApi;

public class ProductApiMappingProfile : Profile
{
    public ProductApiMappingProfile()
    {
        CreateMap<ProductDto, Product>().ReverseMap();
    }
}