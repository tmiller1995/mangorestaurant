using Autofac;
using Autofac.Extensions.DependencyInjection;
using Mango.Services.ProductApi;
using Mango.Services.ProductApi.Core;
using Mango.Services.ProductApi.Infrastructure;
using Mango.Services.ProductApi.Infrastructure.Data;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<ProductApiMappingProfile>();
});

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext(builder.Configuration.GetConnectionString("MangoProductDbContext"));

builder.Host.ConfigureContainer<ContainerBuilder>(containerBuilder =>
{
    containerBuilder.RegisterModule<AutofacCoreModule>();
    containerBuilder.RegisterModule<AutofacInfrastructureModule>();
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    try
    {
        var context = services.GetRequiredService<MangoProductDbContext>();
        context.Database.EnsureCreated();
        context.SeedDatabase();
    }
    catch (Exception e)
    {
        Console.WriteLine(e);
        throw;
    }
}

app.Run();