﻿using Mango.Services.ProductApi.Core.Models;
using Mango.Services.ProductApi.Infrastructure.Data;

namespace Mango.Services.ProductApi;

public static class SeedProductData
{
    public static void SeedDatabase(this MangoProductDbContext dbContext)
    {
        if (dbContext.Product.Any())
        {
            return;
        }

        dbContext.Product.AddRangeAsync(new Product
        {
            Name = "Samosa",
            Price = 15m,
            Description =
                "Praesent scelerisque, mi sed ultrices condimentum, lacus ipsum viverra massa, in lobortis sapien eros in arcu. Quisque vel lacus ac magna vehicula sagittis ut non lacus.<br/>Sed volutpat tellus lorem, lacinia tincidunt tellus varius nec. Vestibulum arcu turpis, facilisis sed ligula ac, maximus malesuada neque. Phasellus commodo cursus pretium.",
            ImageUrl = "https://mangoimagestorage.blob.core.windows.net/mango/14.jpg",
            CategoryName = "Appetizer"
        }, new Product
        {
            Name = "Paneer Tikka",
            Price = 13.99m,
            Description =
                "Praesent scelerisque, mi sed ultrices condimentum, lacus ipsum viverra massa, in lobortis sapien eros in arcu. Quisque vel lacus ac magna vehicula sagittis ut non lacus.<br/>Sed volutpat tellus lorem, lacinia tincidunt tellus varius nec. Vestibulum arcu turpis, facilisis sed ligula ac, maximus malesuada neque. Phasellus commodo cursus pretium.",
            ImageUrl = "https://mangoimagestorage.blob.core.windows.net/mango/12.jpg",
            CategoryName = "Appetizer"
        }, new Product
        {
            Name = "Sweet Pie",
            Price = 10.99m,
            Description =
                "Praesent scelerisque, mi sed ultrices condimentum, lacus ipsum viverra massa, in lobortis sapien eros in arcu. Quisque vel lacus ac magna vehicula sagittis ut non lacus.<br/>Sed volutpat tellus lorem, lacinia tincidunt tellus varius nec. Vestibulum arcu turpis, facilisis sed ligula ac, maximus malesuada neque. Phasellus commodo cursus pretium.",
            ImageUrl = "https://mangoimagestorage.blob.core.windows.net/mango/11.jpg",
            CategoryName = "Dessert"
        }, new Product
        {
            Name = "Pav Bhaji",
            Price = 15m,
            Description =
                "Praesent scelerisque, mi sed ultrices condimentum, lacus ipsum viverra massa, in lobortis sapien eros in arcu. Quisque vel lacus ac magna vehicula sagittis ut non lacus.<br/>Sed volutpat tellus lorem, lacinia tincidunt tellus varius nec. Vestibulum arcu turpis, facilisis sed ligula ac, maximus malesuada neque. Phasellus commodo cursus pretium.",
            ImageUrl = "https://mangoimagestorage.blob.core.windows.net/mango/13.jpg",
            CategoryName = "Entree"
        });

        dbContext.SaveChanges();
    }
}